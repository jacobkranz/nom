package search

import (
	"encoding/json"
	"io/ioutil"
	"sort"

	"github.com/jacobkranz/closestmatch"
)

var (
	subsetSize = []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20}
)

// Search defines our API for this package
type Search interface {
	SearchFile(searchTerm string, filePath string) ([]Item, error)
}

// New returns a struct implementing the Search API
func New() Search {
	return &search{}
}

type search struct {
}

func (s *search) SearchFile(searchTerm string, filePath string) ([]Item, error) {
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		return []Item{}, err
	}

	metaData := &MetaData{}
	err = json.Unmarshal(data, metaData)
	if err != nil {
		return []Item{}, err
	}

	// now we need to create an array of strings with the titles & descriptions in them
	titles := []string{}
	descriptions := []string{}
	for _, i := range metaData.Items {
		titles = append(titles, i.Snippet.Title)
		descriptions = append(descriptions, i.Snippet.Description)
	}

	// cool, now figure out which titles and which descriptions are the closest to the search term
	// note: ClosestN returns a []Pair where Pair{term string, rank int}.
	cm := closestmatch.New(titles, subsetSize)
	titlePairs := cm.ClosestN(searchTerm, len(titles))

	cm = closestmatch.New(descriptions, subsetSize)
	descriptionPairs := cm.ClosestN(searchTerm, len(descriptions))

	// note: this is where I cut corners; I'd actually completely redo the search package so that it would take
	// in a Item and just add the searchRank to it instead of returning the pair because now we have to
	// loop through all the pairs we got, then loop through the metaData.Items to find the correct item = pair,
	// and update search rank.  Annoying.

	for _, titlePair := range titlePairs {
		for i := range metaData.Items {
			if titlePair.Key != metaData.Items[i].Snippet.Title {
				continue
			}

			// we want the highest ranking to be present, whether that's title's rank or description's rank
			if titlePair.Value > metaData.Items[i].SearchRank {
				metaData.Items[i].SearchRank = titlePair.Value
			}
		}
	}

	for _, descriptionPair := range descriptionPairs {
		for i := range metaData.Items {
			if descriptionPair.Key != metaData.Items[i].Snippet.Description {
				continue
			}

			// we want the highest ranking to be present, whether that's title's rank or description's rank
			if descriptionPair.Value > metaData.Items[i].SearchRank {
				metaData.Items[i].SearchRank = descriptionPair.Value
			}
		}
	}

	// ... yea, we're going to let Go do it's sorting magic here for us
	sort.Sort(metaData.Items)

	return metaData.Items, nil
}

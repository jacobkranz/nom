package search

type MetaData struct {
	Items Items `json:"items"`
}

type Item struct {
	Etag       string      `json:"etag"`
	ID         ItemID      `json:"id"`
	Kind       string      `json:"kind"`
	Snippet    ItemSnippet `json:"snippet"`
	SearchRank int
}

type ItemID struct {
	Kind    string `json:"kind"`
	VideoID string `json:"videoId"`
}

type ItemSnippet struct {
	Title                string               `json:"title"`
	ChannelID            string               `json:"channelId"`
	ChannelTitle         string               `json:"channelTitle"`
	Description          string               `json:"description"`
	LiveBroadcastContent string               `json:"liveBroadcastContent"`
	PublishedAt          string               `json:"publishedAt"`
	Thumbnails           map[string]Thumbnail `json:"thumbnails"`
}

type Thumbnail struct {
	Height int    `json:"height"`
	URL    string `json:"url"`
	Width  int    `json:"width"`
}

// for sorting

type Items []Item

func (r Items) Len() int {
	return len(r)
}

func (r Items) Swap(i, j int) {
	r[i], r[j] = r[j], r[i]
}

func (r Items) Less(i, j int) bool {
	return r[i].SearchRank > r[j].SearchRank
}

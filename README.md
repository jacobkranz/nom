# Nom Code Challenge

Jacob Kranz's Code Challenge

## Installation

Just run 
```
go get bitbucket.org/jacobkranz/nom
```
## Usage

There are two main functionalities
```
1) nom fetch "your" "keywords" "here"

2) nom search "your-phrase-here"
```
e.g.
```
nom fetch "tony hawk" "three pointer" "t-shirts"
nom search "lebron james makes godly three pointer and I only like him because he's on the lakers"
```

**Note** Fetch before searching


## Reqs
* Grab youtube metadata for 1000 videos based on keywords
* Rate limit API calls to 5 requests/min
* Save data to a file
* Enable an efficient search via console params that crosses both Title and Description
* Break up the work into 2 different processes and ensure there is no duplication
* Write unit tests (didn't get to this one)
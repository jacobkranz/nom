package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strings"
	"sync"
	"time"

	"bitbucket.org/jacobkranz/nom/search"
	"bitbucket.org/jacobkranz/nom/youtube"
)

const youtubeKeyFile = "youtubeApiKey"
const keywordDirectory = "keywords/"
const searchGoRoutines = 5

var (
	basepath       = os.Getenv("GOPATH") + "/src/bitbucket.org/jacobkranz/nom/"
	keywordPath    = basepath + keywordDirectory
	youtubeKeyPath = basepath + youtubeKeyFile
	searchResults  = 100
)

func main() {

	if len(os.Args) < 2 {
		fmt.Println("Command not found; please reference README for usage")
		return
	}

	switch os.Args[1] {
	case "fetch":
		processFetch(os.Args[2:]) // this is to make what the CLI args are more clean
	case "search":
		processSearch(os.Args[2:])
	default:
		fmt.Printf("Command %s not found; please reference README for usage\n", os.Args[1])
	}
}

// processFetch gets ready everything for fetching our keywords
func processFetch(keywords []string) {
	/*
		- Get developer key from file
			if no key, throw err
		- Get our youtube package
		- Make youtube calls
	*/

	// Get the developer key but if it's first time ask for it && set file
	var youtubeKey string
	youtubeKeyByte, err := ioutil.ReadFile(youtubeKeyPath)
	if err != nil && strings.Contains(strings.ToLower(err.Error()), "no such file or directory") {
		youtubeKey, err = setDeveloperKey()
		if err != nil {
			log.Fatalf("error setting your developer key.  Err: %v", err)
			return
		}
	} else if err != nil {
		log.Fatalf("error opening %s.  Err: %v", youtubeKeyPath, err)
		return
	} else {
		youtubeKey = string(youtubeKeyByte)
	}

	// set our youtube client for getting metadata
	youtubeClient, err := youtube.New(youtube.Config{
		DeveloperKey:         youtubeKey,
		MaxRequestsPerPeriod: 5,
		PeriodDuration:       1 * time.Second,
	})
	if err != nil {
		log.Fatalf("error setting up new youtube client; err: %v", err)
		return
	}

	// concurrently get all the keyword data & save to disk
	wg := &sync.WaitGroup{}
	for _, keyword := range os.Args[2:] {
		wg.Add(1)

		go func(keyword string, wg *sync.WaitGroup) {
			// get data
			fmt.Println("Fetching ", keyword)
			res, err := youtubeClient.GetMetadata(keyword)
			if err != nil {
				log.Fatalf("Error getting meta data for %s; err: %v", keyword, err)
				return
			}

			// make to json
			jsonToStore, err := json.Marshal(res)
			if err != nil {
				log.Fatalf("Error converting results into json; err: %v", err)
				return
			}

			// store!
			err = ioutil.WriteFile(keywordPath+keyword+".json", jsonToStore, 0644)
			if err != nil {
				log.Fatalf("Error saving keyword %s metadata; err: %v", keyword, err)
				return
			}

			fmt.Println("Done fetching ", keyword)
			wg.Done()
		}(keyword, wg)
	}
	wg.Wait()
	fmt.Println("Done")
}

func setDeveloperKey() (string, error) {

	fmt.Print("What is your Youtube Api / Developer Key?\n")

	scanner := bufio.NewScanner(os.Stdin)
	var youtubeKey string

	// get the input
	for scanner.Scan() {
		youtubeKey = scanner.Text() // Println will add back the final '\n'
		break
	}
	if err := scanner.Err(); err != nil {
		return "", err
	}

	// if this is our first time through, chances are we need to create the keywordPath too
	_, err := os.Stat(keywordPath)
	if os.IsNotExist(err) {
		os.Mkdir(keywordPath, 0777)
	} else if err != nil {
		return "", err
	}

	// save to file & return api key
	return youtubeKey, ioutil.WriteFile(youtubeKeyPath, []byte(youtubeKey), 0644)
}

func processSearch(args []string) {
	/*
		The big issue here is going to be scale: assuming we have some keywords saved, we need
		to get the best videos back.  However we can't just put every file into memory and search
		based off that (well, we COULD but probably shouldn't).  So what I think we should do is
		devour every single file in bounded concurrency (e.g. X files at a time), get the rank of each file,
		then have all the files push to a channel where a "master" routine continuely only takes the top results.
		This allows us to put a max on our memory while doing everything concurrently too

		Note: there are a few different fuzzy searching algorithms.  I'm going with N-gram indexing because it:
			1) is a lot better than traditional searching such as with levenshtein
			2) we can alter the "n" and choose to speed up the process w/ less accurate results or slower + more accurate

		There is a really good library at https://github.com/schollz/closestmatch that does something similar but
			it's not exactly what we want because we want the actual ranking in the return.  So I'm going to fork it and make some
			small changes to it, mainly that it'll be returning the []closestmatch.Pair

		Ok!  Let's begin, shall we?
	*/

	// ... just to double check we have a search term
	if len(args) == 0 {
		fmt.Println("Done") // keep it simple
		return
	}

	searchTerm := args[0]
	fmt.Println("Searching with term ", searchTerm)

	/*
		Ok, shall we talk about how this pipeline works?
		1) First, we are going to do some bounded parallelism and boot up searchGoRoutines number of go routines
			These will be listening in for file names
			They will then call searchClient.SearchFile which returns a list of items based off their fuzzy search rank
			The list of items will be pushed onto the next stage of our pipeline: sorting results

		2) In sorting results, we just take our master "results" variable, add the items coming in, sort, take top 100

		It's actually kinda simple but the code is going to look, well... fun.

		Don't worry, I'll walk you through it :)
	*/

	// Get all the files in our keywords directory
	files, err := ioutil.ReadDir(keywordPath)
	if err != nil {
		log.Fatalf("error getting file names from keywords directory; err: %v", err)
		return
	}

	// let's start by making all the chans we need
	filesChan := make(chan string)
	itemsChan := make(chan search.Items, len(files))

	// results will be what's eventually returned
	results := search.Items{}

	searchClient := search.New()

	wg := &sync.WaitGroup{}

	// bounded parallelism (note: calling this "SearchFile routines" for documentation purposes)
	for i := 0; i < searchGoRoutines; i++ {
		wg.Add(1)
		go func() {
			for fileName := range filesChan {
				items, err := searchClient.SearchFile(searchTerm, keywordPath+fileName)
				if err != nil {
					log.Fatalf("error search file %s; err: %v", fileName, err)
					continue
				}
				itemsChan <- items
			}
			wg.Done()
		}()
	}

	// push the files to be devoured & processed by the above declared SearchFile routines
	for _, f := range files {
		filesChan <- f.Name()
	}
	close(filesChan) // we've processed all our files... no more need for it

	/*
		This is something a little funky but basically we want to process items from the itemsChan
			while SearchFile routines are also running so that:
				- we keep the chan length small to not use too much memory,
					otherwise we're basically taking all our data and putting it into memory and that's obv something we
					want to avoid
				- obviously faster results get processed

		So if we do a wg.Wait() on the main process then we have to wait for SearchFile routines to be done
			thus itemsChan gets filled up by every item.  But we need to close itemsChan the moment wg.Wait is done too.sync

		The solution?  Put the wg.Wait() in a go routine and close the chans to our first part of our pipeline right after
	*/
	go func() {
		wg.Wait() // Wait for the SearchFile routines to finish before closing itemsChan
		close(itemsChan)
	}()

	// cool! Let's process our results!!!!
	for items := range itemsChan {
		results = append(results, items...)
		// one SMALL issue is that the same video can come from multiple keywords, so let's delete dupes
		results = deleteDuplicates(results)
		sort.Sort(results)
		results = results[:searchResults]
	}

	// Print our results!
	for i, item := range results {
		fmt.Printf("Result %d\nVideo ID: %s\nTitle: %s\nDescription: %s\n\n", (i + 1), item.ID.VideoID, item.Snippet.Title, item.Snippet.Description)
	}
}

func deleteDuplicates(items search.Items) search.Items {
	// this is actually really expensive and should be redone
	// I know there are better ways of doing this
	resultsMap := map[string]search.Item{}

	for _, item := range items {
		resultsMap[item.ID.VideoID] = item
	}

	// re-use so we don't duplicate memory... again
	items = search.Items{}
	for _, item := range resultsMap {
		items = append(items, item)
	}

	return items
}

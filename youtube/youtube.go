package youtube

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"sync"
	"time"

	"google.golang.org/api/googleapi/transport"
	youtubeClient "google.golang.org/api/youtube/v3"
)

// could be in config, but this is generally going to always be static
const resultsPerPage = 50
const totalResults = 1000

// Youtube defines our interface for this package
type Youtube interface {
	GetMetadata(keyword string) (metaData *MetaData, err error)
	Close()
}

// Config represents our configuration for this package
type Config struct {
	DeveloperKey         string
	MaxRequestsPerPeriod int
	PeriodDuration       time.Duration
}

// New returns a struct that implements the Youtube api
func New(conf Config) (Youtube, error) {

	client := &http.Client{
		Transport: &transport.APIKey{Key: conf.DeveloperKey},
	}

	service, err := youtubeClient.New(client)
	if err != nil {
		log.Fatalf("Error creating new YouTube client: %v", err)
		return nil, err
	}

	y := &youtube{
		client:      service,
		ticker:      time.NewTicker(conf.PeriodDuration),
		errChan:     make(chan error, 1),
		mutex:       &sync.Mutex{},
		maxRequests: conf.MaxRequestsPerPeriod,
	}

	// so what in the world are we doing here?
	// Because we can fetch multiple keywords at once and all concurrently,
	// we are gong to have a counter on youtube for the number of requests
	// over the last tick.  Once the tick occurs, clear out the number of requests.
	// The reason for the timer method is that it'll also listen to errChan to see if it should
	// close out the ticker or not.
	go timer(y)

	return y, nil
}

type youtube struct {
	client             *youtubeClient.Service
	requestsInLastTick int // this is our increment to how many times we've done
	maxRequests        int // number of requests to youtube we can make per tick
	mutex              *sync.Mutex
	ticker             *time.Ticker
	errChan            chan error // to kill all of our channels
}

func timer(y *youtube) {
	for {
		select {
		case <-y.ticker.C:
			// update the requestsInLastTick to be 0
			y.mutex.Lock()
			y.requestsInLastTick = 0
			y.mutex.Unlock()
		case <-y.errChan:
			y.ticker.Stop()
		}
	}
}

func (y *youtube) Close() {
	y.errChan <- errors.New("CLOSING ALL OPEN ROUTINES")
}

func (y *youtube) GetMetadata(keyword string) (*MetaData, error) {

	var metaData *MetaData

	for {
		// we either have totalResults OR we have as many results as possible ()
		if metaData != nil && (metaData.PageInfo.ListedResults >= totalResults || metaData.PageInfo.ListedResults >= metaData.PageInfo.TotalSearchResults) {
			break
		}

		allowedToMakeNextRequest := false

		// check to see if we are allowed to make a request
		y.mutex.Lock()
		if y.requestsInLastTick < y.maxRequests {
			y.requestsInLastTick++
			allowedToMakeNextRequest = true
		}
		y.mutex.Unlock()

		if allowedToMakeNextRequest {
			nextPageToken := ""
			if metaData != nil {
				nextPageToken = metaData.NextPageToken
			}

			data, err := y.list(keyword, nextPageToken)
			if err != nil {
				return nil, err
			}

			// if this is our first request, just set the metadata to be the data
			if metaData == nil {
				metaData = data
			} else {
				// update metadata with more results
				metaData.Items = append(metaData.Items, data.Items...)
				metaData.NextPageToken = data.NextPageToken
				metaData.PageInfo.ListedResults = len(metaData.Items)
			}
		} else {
			// allowedToMakeNextRequest is false so wait for the ticker to come up to continue with the for loop
			<-y.ticker.C
		}
	}

	// since we always get the last 50 and for some reason youtube sometimes sends <50 items back so we go over totalResults sometimes
	// e.g. we have 950 items, 32 get returned, so we do another 50 and now we're at 1032
	// thus, we need to trim to totalResults

	// make sure we don't do out of bounds when doing metaData.Items[:totalResults] so check to see
	// if number of search results is less than that
	results := totalResults
	if metaData.PageInfo.TotalSearchResults < totalResults {
		results = metaData.PageInfo.TotalSearchResults
	}

	metaData.Items = metaData.Items[:results]
	metaData.PageInfo.ListedResults = len(metaData.Items)

	return metaData, nil
}

func (y *youtube) list(keyword string, nextPageToken string) (*MetaData, error) {

	// Make the API call to YouTube.
	call := y.client.Search.List("id,snippet").
		Q(keyword).
		MaxResults(50).
		Type("video") // ignore channels & playlists

	if nextPageToken != "" {
		call = call.PageToken(nextPageToken)
	}

	// get the response
	response, err := call.Do()
	if err != nil {
		log.Fatalf("Error making search API call: %v", err)
		return nil, err
	}

	// ok, so our response is a SearchListResponse which won't marshal into our package structs that we want
	// (and we want them as our structs because we want to remove info from them if needed)
	// so we are going to first make it into json then unmarshal it into our structs
	b, err := response.MarshalJSON()
	if err != nil {
		log.Fatalf("Err marshalling response: %v", err)
		return nil, err
	}

	data := &MetaData{}
	err = json.Unmarshal(b, data)
	if err != nil {
		return nil, err
	}

	// cool!.... cool, cool, cool!
	return data, nil
}

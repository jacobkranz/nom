package youtube

type MetaData struct {
	Etag          string           `json:"etag"`
	Items         []Item           `json:"items"`
	Kind          string           `json:"kind"`
	PageInfo      MetaDataPageInfo `json:"pageInfo"`
	RegionCode    string           `json:"regionCode"`
	NextPageToken string           `json:"nextPageToken"`
	PrevPageToken string           `json:"prevPageToken"`
}

type MetaDataPageInfo struct {
	ListedResults      int `json:"ListedResults"`
	TotalSearchResults int `json:"totalResults"`
}

type Item struct {
	Etag    string      `json:"etag"`
	ID      ItemID      `json:"id"`
	Kind    string      `json:"kind"`
	Snippet ItemSnippet `json:"snippet"`
}

type ItemID struct {
	Kind    string `json:"kind"`
	VideoID string `json:"videoId"`
}

type ItemSnippet struct {
	Title                string               `json:"title"`
	ChannelID            string               `json:"channelId"`
	ChannelTitle         string               `json:"channelTitle"`
	Description          string               `json:"description"`
	LiveBroadcastContent string               `json:"liveBroadcastContent"`
	PublishedAt          string               `json:"publishedAt"`
	Thumbnails           map[string]Thumbnail `json:"thumbnails"`
}

type Thumbnail struct {
	Height int    `json:"height"`
	URL    string `json:"url"`
	Width  int    `json:"width"`
}
